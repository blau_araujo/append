# append

Pure bash append tool

## Description

Appends a line of text to a file. If file does not exist, it can be created.

## Install

Clone this project, copy the script `append` to your PATH and make it executable:

```
chmod +x append
```

## Usage

`append [-l \"<text>\" -f <file>][-h]`


### Example:

```
:~$ append -l "some text" -f ~/test/file.txt
```

For interactive mode, run with no arguments.


```
:~$ append

Text to append: some text
File to append to: ~/test/tt9.txt

Appending 'some text' to '/home/gda/test/tt9.txt'
Confirm? (s/N) s

File contents...

test
other test
some text

```

* File name has auto-completion
* Quits intercative mode with blank text or file name

